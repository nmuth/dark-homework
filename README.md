# dark homework

https://wonderful-montalcini-667921.netlify.com

## Approach

I used this project as an opportunity to finally take a deep dive into React
Hooks. I learned a lot implementing it -- hooks are a really interesting way to
build abstractions on top of imperative APIs like `getUserMedia`.

Off the top of my head, I didn't know much about hooks or the webcam API, so I
went a-Googlin' to learn about the best practices for both. Here are some of the
most useful URLs I found:

- [official draw-video-frame-to-a-canvas example from WebRTC](https://github.com/webrtc/samples/blob/gh-pages/src/content/getusermedia/canvas/js/main.js)
- [React Hooks documentation](https://reactjs.org/docs/hooks-custom.html)
- [this wonderful Dan Abramov article about using hooks to build abstractions over non-declarative APIs](https://overreacted.io/making-setinterval-declarative-with-react-hooks/)

## Architecture

The render tree of the app looks like this:

- [App](./src/components/App.js)
  - [LoadingText](./src/components/LoadingText.js) 
  - [VideoFrameCapturer](./src/components/VideoFrameCapturer.js) 
  - [OutputContainer](./src/components/OutputContainer.js)
    - [LiveVideoStream](./src/components/LiveVideoStream.js)
    - [CapturedFrames](./src/components/CapturedFrames.js)

Each of those files should have their own comments, but the TL;DR is:

1. `App` is the top-level container (duh). It holds app-wide state and uses the
   [`useWebcam`](./src/hooks/useWebcam.js) hook to gain control of the video
   stream.
2. `VideoFrameCapturer` is an offscreen canvas that captures frames from the
   video stream and spits them back out as data URLs. The interval is scheduled
   with the [`useInterval`](./src/hooks/useInterval) hook.
3. `LiveVideoStream` contains the `<video/>` element that shows the live video feed.
4. `CapturedFrames` displays the output.

## Run, Build, Deploy

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). To get it up and running on localhost, just type:

```
$ yarn install
$ yarn start
```

### `yarn start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

Sadly, there are no tests here.

### `yarn build`

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
