import React from 'react'

export const CapturedFrames = ({ frames = [], aspectRatio = 1, width = 150 }) => {
  const images = frames.map((frame, idx) => (
    <img key={idx}
         src={frame}
         alt={`camera frame ${idx}`}
         style={{ width, height: width / aspectRatio}}/>
  ))

  return (
    <>
      {images}
    </>
  )
}
