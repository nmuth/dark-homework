import React from 'react'

export function LoadingText({ hasVideo = false, videoDenied = false } = {}) {
  if (hasVideo) {
    return null
  }

  const message = videoDenied
            ? `you've gotta allow the camera or this won't work`
            : 'loading'

  return (
    <div style={{ width: '100vw', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <h1 style={{ textAlign: 'center' }}>{message}</h1>
    </div>
  )
}
