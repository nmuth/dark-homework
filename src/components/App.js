import React, { useState } from 'react'
import { useWebcam } from '../hooks'
import { CapturedFrames } from './CapturedFrames'
import { LiveVideoStream } from './LiveVideoStream'
import { LoadingText } from './LoadingText'
import { OutputContainer } from './OutputContainer'
import { VideoFrameCapturer } from './VideoCapturer'
import './App.css'

/**
 * Main application container. Holds application-level state -- namely the video
 * stream, video element, and the captured frames -- and distributes it to the
 * appropriate children.
 */
export function App() {
  const [videoEl, setVideoEl] = useState(null)
  const [frames, setFrames] = useState([])
  const video = useWebcam()

  const onCaptureFrame = frame => setFrames([frame].concat(frames).slice(0, 32))
  const aspectRatio = video.settings.width / video.settings.height

  return (
    <div className="App">
      <LoadingText hasVideo={!!video.stream} videoDenied={video.denied}/>
      <VideoFrameCapturer interval={1000} videoEl={videoEl} onCaptureFrame={onCaptureFrame}/>

      <OutputContainer>
        <LiveVideoStream onVideoChange={setVideoEl} stream={video.stream}/>
        <CapturedFrames frames={frames} aspectRatio={aspectRatio}/>
      </OutputContainer>
    </div>
  )
}
