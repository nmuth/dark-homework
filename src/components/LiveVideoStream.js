import React, { useRef, useEffect } from 'react'

/**
 * Display the given media stream as a live, autoplaying video. Also gets a
 * reference to the <video/> element and passes it to the parent so that the
 * parent can do things like play/pause or... capture a frame every second.
 *
 * Memoized because:
 *
 * 1. Both the props are pointers, so it's efficient and safe
 * 2. Unnecessary re-renders cause the video stream to stutter
 */
export const LiveVideoStream = React.memo(({ onVideoChange = () => {}, stream = null }) => {
  const videoRef = useRef(null)
  const videoEl = videoRef.current

  useEffect(() => {
    onVideoChange(videoEl)

    if (videoEl) {
      videoEl.srcObject = stream
    }
  }, [videoEl, onVideoChange, stream])

  return (
    <video ref={videoRef} autoPlay style={{width: 150}}/>
  )
})
