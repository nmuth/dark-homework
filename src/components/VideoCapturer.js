import React, { useRef } from 'react'
import { useInterval } from '../hooks'

/**
 * Offscreen canvas that takes in a <video/> element and captures a frame from
 * it on the given interval (default 1 second).
 */
export const VideoFrameCapturer = React.memo(({ videoEl = null, interval = 1000, onCaptureFrame = () => {} }) => {
  const canvasRef = useRef(null)
  const canvas = canvasRef.current
  const ctx = canvas && canvas.getContext('2d')

  useInterval(() => {
    if (!ctx || !videoEl) {
      return
    }

    ctx.drawImage(videoEl, 0, 0, canvas.width, canvas.height)
    onCaptureFrame(canvas.toDataURL('image/png'))
  }, interval)

  return (
    <div>
      <canvas ref={canvasRef} style={{ position: 'absolute', left: -1000, top: -1000, width: 128, height: 128 }}/>
    </div>
  )
})
