import React from 'react'

export const OutputContainer = ({ children }) => (
  <div style={{ display: 'flex', flexWrap: 'wrap', margin: '1em' }}>
    {children}
  </div>
)
