import { useEffect, useState } from 'react'

/**
 * Effect to handle getting the user's webcam. Returns the video stream, the
 * video settings (e.g. width, height, framerate), and a flag to indicate
 * whether the request was denied.
 */
export const useWebcam = () => {
  const [{ stream, settings }, setVideo] = useState({ stream: null, settings: { width: 128, height: 128 } })
  const [denied, setDenied] = useState(false)

  useEffect(() => {
    async function getWebcam() {
      if (stream || denied) {
        return
      }

      try {
        const stream = await navigator.mediaDevices.getUserMedia({ audio: false, video: true })
        const settings = stream.getVideoTracks()[0].getSettings()
        setVideo({ stream, settings })
      } catch (err) {
        console.warn('user denied webcam access:', err)
        setDenied(true)
      }
    }

    getWebcam()
  }, [stream, setVideo, denied, setDenied])

  return { stream, settings, denied }
}
